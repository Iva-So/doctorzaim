$(document).ready(function () {

    $('.tpl-hamburger, .tpl-mobile-background').click(function () {
        $('.tpl-hamburger').toggleClass('active');
        $('.tpl-header__dropdown').toggleClass('active');
        $('.tpl-mobile-background').toggleClass('active');
    });

    var $calcForm = $('.tpl-general-calculation');
    $calcForm.each(function (index, elem) {
        var calc = new Calc($(elem));
        calc.init();
    });

    $('.js-confirm').click(function () {
        $('#w0').submit();
    });

    $('.tpl-calculation__input-control').on('click', function (e) {
        let input = $('#form-amount-control');
        let stepAmount = input.attr('step');
        let minAmount = input.attr('min');
        let maxAmount = input.attr('max');

        let currentVal = Number(input.val().replace(" ", ""));
        if ($(this).is('.js-amount-down')) {
            currentVal -= Number(stepAmount);
        } else if ($(this).is('.js-amount-up')) {
            currentVal += Number(stepAmount);
        }
        if (currentVal <= maxAmount && currentVal >= minAmount)
            input.val(currentVal.toLocaleString('en-US').replace(",", " "));
        $('#amount-input').val(currentVal);
        input.trigger('change');
    });
});

function maskPhone(e) {
    const mask = /\+7 \(\d{3}\) \d{3} \d{2} \d{2}/;
    var valSize = e.target.value.trim().replace(/\D/g, "").length;
    e = e || window.event;
    var key = e.keyCode || e.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\+/;
    if (!regex.test(key)) {
        e.returnValue = false;
        if (e.preventDefault) e.preventDefault();
    } else {
        if (valSize !== 0 && key === "+") {
            e.returnValue = false;
            return;
        }
        if (valSize === 0) {
            if (key === "8" || key === "7") {
                e.target.value = "+7";
                e.returnValue = false;
            } else if (key === "9") {
                e.target.value = "+7 (9";
                e.returnValue = false;
            } else if (key !== "+") {
                e.target.value = "+7 (9";
            } else if (key === "+" && e.target.value === "+") {
                e.returnValue = false;
            }
        } else if (valSize === 1) {
            e.target.value = "+7 (9";
            if (key === "9") {
                e.returnValue = false;
            }
        } else if (valSize === 4) {
            if (e.target.value.slice(-1) === ")") {
                e.target.value = e.target.value.trim() + " ";
            } else if (e.target.value.slice(-1) === " ") {
                return;
            } else e.target.value = e.target.value.trim() + ") ";
        } else if (valSize === 7 || valSize === 9) {
            if (e.target.value.slice(-1) === " ") {
                return;
            } else e.target.value = e.target.value.trim() + " ";
        } else if (valSize === 11) {
            e.returnValue = false;
        }
    }
}

function onPastePhone(e) {
    e.preventDefault();
    const mask = /(7|8)(9\d{2})(\d{3})(\d{2})(\d{2})/;
    var phone = e.clipboardData.getData('text/plain').replace(/\D/g, "");
    if (!mask.test(phone)) {
        e.returnValue = false;
        return;
    }
    var matched = phone.match(mask);
    e.target.value = "+7 (" + matched[2] + ") " + matched[3] + " " + matched[4] + " " + matched[5];
    e.returnValue = false;
}
