function Calc($form) {
    this.sumDisplaySelector = '#amount-input';
    this.sumSliderSelector = '#amount-slider';

    this.$form = $form;
    this.$sumDisplay = $form.find(this.sumDisplaySelector).filter('input');
    this.$sumSlider = $form.find(this.sumSliderSelector);

    this.oldSum = 0;
    this.minSum = this.$sumSlider.attr('min');
    this.maxSum = this.$sumSlider.attr('max');
    this.sumStep = this.$sumSlider.attr('step');
}

Calc.prototype.init = function () {
    let _this = this;

    this.$form.on('change', _this.sumSliderSelector, function () {
        _this.recalculate();
    });
    this.$form.on('input', _this.sumSliderSelector, function () {
        _this.recalculate();
    });
    this.$form.find(_this.sumSliderSelector).rangeslider({
        polyfill: false
    });
    this.$sumDisplay.on('change', function (e) {
        _this.recalculate();
    });
    this.$sumDisplay.on('keydown', function (e) {
        if( e.keyCode == 13) {
            e.preventDefault();
            $(this).trigger('change');
            $(this).trigger('blur');
        }
    });
    this.oldSumm = this.$sumSlider.val();
    this.recalculate();
};

Calc.getApprovalPercent = function (currentSum, sumInterval) {
    let result = 0;

    for (const sum in sumInterval) {
        if (sumInterval.hasOwnProperty(sum)) {
            if (+currentSum <= +sum) {
                result = sumInterval[sum].approvalPercent;
                break;
            }
        }
    }

    return result;
};

Calc.formatSum = function (sum) {
    return (sum || 0).toString().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ');
};

Calc.prototype.clearSum = function (string) {
    let inputSum = string || '';
    let regex = new RegExp('[^0-9]', 'g');
    inputSum = inputSum.replace(regex, '');
    if (inputSum.length === 0) {
        inputSum = this.oldSum;
    }
    if (+inputSum < +this.minSum) {
        inputSum = this.minSum;
    }
    if (+inputSum > +this.maxSum) {
        inputSum = this.maxSum;
    }
    return inputSum;
};

Calc.prototype.recalculate = function () {
    // Чтобы не повторных срабатываний если не нужны
    if (this.isRecalculating === true) {
        return;
    }

    // Собираем данные
    let sliderSum = this.$sumSlider.val();
    let inputSum = this.clearSum(this.$sumDisplay.val());

    let sumDisplay = (inputSum !== this.oldSum) ? sliderSum : inputSum;

    this.isRecalculating = true;
    this.oldSumm = sumDisplay;
    this.$sumSlider.val(sumDisplay);

    $(this.$sumDisplay).html(Calc.formatSum(sumDisplay));

    $(this.$sumSlider).trigger('change');

    // высчитывается итоговая сумма с 50000 с комиссией, комиссия = 9%
    if (sumDisplay > 50000) {
        var commission = sumDisplay * 0.09;
        var total = parseInt(sumDisplay) + parseInt(commission);
        $("#amount-total").html(new Intl.NumberFormat('ru-RU').format(total));
        $("#amount-commission").html(commission);
    } else {
        $("#amount-total").html(new Intl.NumberFormat('ru-RU').format(sumDisplay));
        $("#amount-commission").html(0);
    }
    
    this.isRecalculating = false;
    // отображаем все в форме
    sumDisplay = this.constructor.formatSum(sumDisplay);
    this.$sumDisplay.val(sumDisplay);
    $("#amount-take").html(sumDisplay);

};
